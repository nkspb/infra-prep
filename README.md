infra-prep
=========

**Infrastructure for pg-app:**<br /><br />
_ansible_ - playbooks for installing srv-node main components<br />
_k8s_manifests_ - one-time manifests for k8s resources<br />
_monitoring_ - docker/k8s manifests for monitoring<br />
_ssh_ - public ssh key to use for deployed hosts<br />
_terraform_ - deploy k8s cluster and srv-nodes<br />
<br />
**Deployment steps:**
<br />
1. Set storage keys in _infra-prep/terraform/main.tf_:<br />
```
backend "s3" {
    ...
    access_key = "X" 
    secret_key = "X" 
```

2. Set access variables in _infra-prep/terraform/variables.tf_

3. Apply the manifests:<br />
```
terraform plan
terraform apply
```

4. Apply ansible playbook to install srv-node components<br />
```ansible-playbook playbook-main.yaml```

5. Set telegram bot user ID and token in _monitoring/srv-node/prom-grafana/docker-compose.yaml:_<br />
```alertmanager-bot:<br />
command:
      - --telegram.admin=X
      - --telegram.token=X
```

6. Login to Grafana and import metrics from _/opt/srv-monitoring/prom-grafana/grafana/provisioning/dashboard.json_

**App Status Code:** probe_http_status_code{instance ="http://k8s-node-ip:30000"}<br />
**App Probe Duration:** probe_duration_seconds{instance="http://k8s-node-ip:30000"}<br />
**Number of replicas:** kube_deployment_spec_replicas{deployment="pg-app"}<br />
kube_statefulset_status_replicas{statefulset="pg-statefulset"}<br />
**CPU srv-node**: _100 - 100 * (avg(irate(node_cpu_seconds_total{instance="exporter-srv:9100", mode="idle"}[1m])))_<br />
**RAM (srv-node):** node_memory_Active_bytes{instance="exporter-srv:9100"}/node_memory_MemTotal_bytes{instance="exporter-srv:9100"}*100<br />
**Free Space (srv-node):** node_filesystem_free_bytes{instance="exporter-srv:9100"}<br />
**CPU (k8s):** 100 - 100 * (avg(irate(node_cpu_seconds_total{instance="k8s-node-ip:31500", mode="idle"}[1m])))<br />
**RAM (k8s)**: node_memory_Active_bytes{instance="k8s-node-ip:31500"}/node_memory_MemTotal_bytes{instance="k8s-node-ip:31500"}<br />
**PVC requests**: kube_persistentvolumeclaim_resource_requests_storage_bytes<br />

7. App is started automatically by _playbook-main.yaml_.<br />
Pipeline runs on commit to _gitlab.com/nkspb/pg-docker-app.git_
```
cd pg-docker-app/helm/postgres/charts/postgres
helm upgrade --install --create-namespace --namespace pg-app postgres .

cd pg-docker-app/helm/postgres/charts/pg-app
helm upgrade --install --create-namespace --namespace pg-app pg-app --set appVersion=X .
```
where _appVersion_ is the version on Docker Hub:<br />
https://hub.docker.com/repository/docker/nkom/pg-app<br />


| Component | Address |
| ------ | ------ |
| App | k8s-node-ip:30000 |
| Grafana | srv-node-ip:3000 |
| Prometheus | srv-node-ip:9090 |
| Alertmanager | srv-node-ip:9093 |
| Kibana | srv-node-ip:5601 |
| Elasticsearch | srv-node-ip:9200 |






