# terraform (yandex-cloud)
Deploy infrastructure for pg-app and run ansible automatically to finish setup

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.5.0 |
| <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) | 0.97.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_local"></a> [local](#provider\_local) | 2.4.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.2.1 |
| <a name="provider_yandex"></a> [yandex](#provider\_yandex) | 0.97.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_k8s_cluster"></a> [k8s\_cluster](#module\_k8s\_cluster) | ./modules/k8s_cluster | n/a |
| <a name="module_srv_node"></a> [srv\_node](#module\_srv\_node) | ./modules/srv_node | n/a |

## Resources

| Name | Type |
|------|------|
| [local_file.inventory](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [null_resource.ansible](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [yandex_vpc_network.cloud_1_net](https://registry.terraform.io/providers/yandex-cloud/yandex/0.97.0/docs/resources/vpc_network) | resource |
| [yandex_vpc_subnet.cloud_1_subnet_1](https://registry.terraform.io/providers/yandex-cloud/yandex/0.97.0/docs/resources/vpc_subnet) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloud_id"></a> [cloud\_id](#input\_cloud\_id) | Cloud id for the project | `string` | n/a | yes |
| <a name="input_folder_id"></a> [folder\_id](#input\_folder\_id) | Folder id for the project | `string` | n/a | yes |
| <a name="input_k8s_version"></a> [k8s\_version](#input\_k8s\_version) | Kubernetes version to deploy | `string` | n/a | yes |
| <a name="input_token"></a> [token](#input\_token) | Token to access cloud | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | Availability zone for VM | `string` | `"ru-central1-a"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_k8s_cluster_id"></a> [k8s\_cluster\_id](#output\_k8s\_cluster\_id) | n/a |
| <a name="output_k8s_cluster_name"></a> [k8s\_cluster\_name](#output\_k8s\_cluster\_name) | n/a |
| <a name="output_master_external_ip"></a> [master\_external\_ip](#output\_master\_external\_ip) | n/a |
| <a name="output_master_internal_ip"></a> [master\_internal\_ip](#output\_master\_internal\_ip) | n/a |
| <a name="output_srv_node_external_ipv4"></a> [srv\_node\_external\_ipv4](#output\_srv\_node\_external\_ipv4) | n/a |
| <a name="output_srv_node_hostname"></a> [srv\_node\_hostname](#output\_srv\_node\_hostname) | n/a |
| <a name="output_srv_node_id"></a> [srv\_node\_id](#output\_srv\_node\_id) | n/a |
| <a name="output_srv_node_internal_ipv4"></a> [srv\_node\_internal\_ipv4](#output\_srv\_node\_internal\_ipv4) | n/a |
