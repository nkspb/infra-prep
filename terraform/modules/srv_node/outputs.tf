output "instance_id" {
  value = yandex_compute_instance.compute_instance.id
}

output "instance_hostname" {
  value = yandex_compute_instance.compute_instance.hostname
}

output "instance_ipv4_internal" {
  value = yandex_compute_instance.compute_instance.network_interface.0.ip_address
} 

output "instance_ipv4_external" {
  value = yandex_compute_instance.compute_instance.network_interface.0.nat_ip_address
}