# Module to create new yandex srv node instance

terraform {
  required_version = "1.8.3"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.117.0"
    }
  }
}

data "yandex_compute_image" "vm_image" {
  family = var.vm_family
}

resource "yandex_compute_instance" "compute_instance" {
  name = var.vm_name
  zone = var.vm_zone

  hostname = "srv"
  resources {
    cores  = 4
    memory = 8
    core_fraction = 20
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.vm_image.id
      size = 15
    }
  }

  network_interface {
    subnet_id = var.vm_subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("/home/nkom/.ssh/id_rsa.pub")}"
  }
} 
