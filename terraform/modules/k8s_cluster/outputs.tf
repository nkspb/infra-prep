output "cluster_id" {
  value = yandex_kubernetes_cluster.k8s_cluster.id
}

output "cluster_name" {
  value = yandex_kubernetes_cluster.k8s_cluster.name
}

output "master_internal_ip" {
  value = yandex_kubernetes_cluster.k8s_cluster.master[0].internal_v4_address
}

output "master_external_ip" {
  value = yandex_kubernetes_cluster.k8s_cluster.master[0].external_v4_address
}
