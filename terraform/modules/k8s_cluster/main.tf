terraform {
  required_version = "1.8.3"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.117.0"
    }
  }
}

locals {
  k8s_sa = "k8s-sa"
}

resource "yandex_iam_service_account" "k8s_sa" {
  name        = local.k8s_sa
  description = "K8S regional service account"
}

resource "yandex_resourcemanager_folder_iam_binding" "k8s_clusters_agent" {
  # Assign k8s.clusters.agent role to service account
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  members = [
    "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "vpc_public_admin" {
  # Assign vpc.publicAdmin role to service account
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  members = [
    "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images_puller" {
  # Assign  container-registry.images.puller to service account
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  members = [
    "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
  ]
}

resource "yandex_kms_symmetric_key" "k8s_kms_key" {
  name              = "k8s_kms_key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}

resource "yandex_vpc_security_group" "k8s_main_sg" {
  name        = "k8s-main-sg"
  description = "Security Group rules"
  network_id  = var.cluster_network_id

  egress {
    protocol       = "ANY"
    description    = "Allow any outgoing connections"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }

  ingress {
    protocol       = "ANY"
    description    = "Allow any incoming connections"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "yandex_kubernetes_cluster" "k8s_cluster" {
  name        = "k8s-cluster"
  description = "Kubernetes cluster with 1 master node and regular node"

  network_id = var.cluster_network_id

  service_account_id      = yandex_iam_service_account.k8s_sa.id
  node_service_account_id = yandex_iam_service_account.k8s_sa.id
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.k8s_clusters_agent,
    yandex_resourcemanager_folder_iam_binding.vpc_public_admin,
    yandex_resourcemanager_folder_iam_binding.images_puller
  ]
  
  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s_kms_key.id
  }

  master {
    version = var.k8s_version
    zonal {
      zone      = var.cluster_subnet_zone
      subnet_id = var.cluster_subnet_id
    }

    public_ip = true

    security_group_ids = [yandex_vpc_security_group.k8s_main_sg.id]

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }
  }

  release_channel = "STABLE"
  network_policy_provider = "CALICO"
}
