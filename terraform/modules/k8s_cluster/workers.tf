resource "yandex_kubernetes_node_group" "worker-nodes" {
  cluster_id = yandex_kubernetes_cluster.k8s_cluster.id
  name       = "worker-nodes"
  version    = var.k8s_version

  scale_policy {
    fixed_scale {
      size = "1"
    }
  }

  instance_template {
    platform_id = "standard-v1"
    network_interface {
      nat                = true
      subnet_ids         = [var.cluster_subnet_id]
      security_group_ids = [yandex_vpc_security_group.k8s_main_sg.id]
    }

    resources {
      memory = 2
      cores  = 2
    }

    container_runtime {
        type = "containerd"
    }
    
    metadata = {
      ssh-keys = "ubuntu:${file("/home/nkom/.ssh/id_rsa.pub")}"
    }

  }
}
