## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.2.3 |
| <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) | 0.82.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_yandex"></a> [yandex](#provider\_yandex) | 0.82.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [yandex_iam_service_account.k8s_sa](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/iam_service_account) | resource |
| [yandex_kms_symmetric_key.k8s_kms_key](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/kms_symmetric_key) | resource |
| [yandex_kubernetes_cluster.k8s_cluster](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/kubernetes_cluster) | resource |
| [yandex_kubernetes_node_group.worker-nodes](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/kubernetes_node_group) | resource |
| [yandex_resourcemanager_folder_iam_binding.images_puller](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/resourcemanager_folder_iam_binding) | resource |
| [yandex_resourcemanager_folder_iam_binding.k8s_clusters_agent](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/resourcemanager_folder_iam_binding) | resource |
| [yandex_resourcemanager_folder_iam_binding.vpc_public_admin](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/resourcemanager_folder_iam_binding) | resource |
| [yandex_vpc_security_group.k8s_main_sg](https://registry.terraform.io/providers/yandex-cloud/yandex/0.82.0/docs/resources/vpc_security_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_network_id"></a> [cluster\_network\_id](#input\_cluster\_network\_id) | Name of cluster network to use | `string` | n/a | yes |
| <a name="input_cluster_subnet_id"></a> [cluster\_subnet\_id](#input\_cluster\_subnet\_id) | Cluster subnet id | `string` | n/a | yes |
| <a name="input_cluster_subnet_zone"></a> [cluster\_subnet\_zone](#input\_cluster\_subnet\_zone) | Cluster subnet zone | `string` | n/a | yes |
| <a name="input_folder_id"></a> [folder\_id](#input\_folder\_id) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_master_external_ip"></a> [master\_external\_ip](#output\_master\_external\_ip) | n/a |
| <a name="output_master_internal_ip"></a> [master\_internal\_ip](#output\_master\_internal\_ip) | n/a |
