# Name of cluster network
variable "cluster_network_id" {
  type        = string
  description = "Name of cluster network to use"
}

variable "cluster_subnet_id" {
  type = string
  description = "Cluster subnet id"
}

variable "cluster_subnet_zone" {
  type = string
  description = "Cluster subnet zone"
}

variable "folder_id" {
  type = string
  description = "ID of resources folder"
}

variable "k8s_version" {
  type = string
  description = "Kubernetes version to deploy"
}