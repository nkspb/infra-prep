variable "zone" {
  description = "Availability zone for VM"
  type        = string
  default     = "ru-central1-a"
}

variable "folder_id" {
  description = "Folder id for the project"
  type        = string
}

variable "cloud_id" {
  description = "Cloud id for the project"
  type        = string
}

variable "token" {
  description = "Token to access cloud"
  type        = string
}

variable "k8s_version" {
  type = string
  description = "Kubernetes version to deploy"
}