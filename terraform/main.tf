terraform {
  required_version = "1.8.3"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.117.0"
    }
  }

  backend "s3" {
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }
    region                      = "ru-central1-a"
    bucket                      = "nkom-bucket-1"
    key                         = "state_1.tfstate"
    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true
    access_key = "YCAJELSrk1mxM8g8iONUMmRtA"
    secret_key = "YCPm0mgU-KPLANkY8tVGv9INkeHkQPYXXRRyJRQq"
  }
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

resource "yandex_vpc_network" "cloud_1_net" {
  name        = "cloud_1_net"
  description = "Network for k8s and srv nodes"
}

resource "yandex_vpc_subnet" "cloud_1_subnet_1" {
  name           = "cloud_1_subnet_1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.cloud_1_net.id
  v4_cidr_blocks = ["10.1.0.0/16"]
  description    = "Subnet for k8s and srv nodes"
}

module "k8s_cluster" {
  source       = "./modules/k8s_cluster"
  cluster_network_id = yandex_vpc_network.cloud_1_net.id
  cluster_subnet_id = yandex_vpc_subnet.cloud_1_subnet_1.id
  cluster_subnet_zone = yandex_vpc_subnet.cloud_1_subnet_1.zone
  folder_id = var.folder_id
  k8s_version = var.k8s_version
}

module "srv_node" {
  source       = "./modules/srv_node"
  vm_name      = "srv-node"
  vm_zone      = "ru-central1-a"
  vm_family    = "ubuntu-2204-lts"
  vm_subnet_id = yandex_vpc_subnet.cloud_1_subnet_1.id
}

# generate ansible inventory
resource "local_file" "inventory" {
  content  = templatefile("templates/inventory.tftpl", 
  {
    ipv4_public = module.srv_node.instance_ipv4_external
    hostname = module.srv_node.instance_hostname
  })
  filename = "${path.module}/../ansible/inventory.yaml"
}

# resource "null_resource" "ansible" {
#   provisioner "local-exec" {
#     working_dir = "../ansible"
#     command = "ansible-playbook playbook-main.yaml"
#   } 
#   depends_on = [module.k8s_cluster, module.srv_node]
# }
