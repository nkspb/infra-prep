output "srv_node_id" {
    value = module.srv_node.instance_id
}

output "srv_node_hostname" {
    value = module.srv_node.instance_hostname
}

output "srv_node_internal_ipv4" {
    value = module.srv_node.instance_ipv4_internal
}

output "srv_node_external_ipv4" {
    value = module.srv_node.instance_ipv4_external
}

output "k8s_cluster_id" {
    value = module.k8s_cluster.cluster_id
}

output "k8s_cluster_name" {
    value = module.k8s_cluster.cluster_name
}

output "master_internal_ip" {
  value = module.k8s_cluster.master_internal_ip
}

output "master_external_ip" {
  value = module.k8s_cluster.master_external_ip
}
