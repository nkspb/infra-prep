kubectl
=========

Role for yandex-cloud cli installation and configuration

Example Playbook
----------------

    - hosts: srv
      become: yes
      roles:
        - yc

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb