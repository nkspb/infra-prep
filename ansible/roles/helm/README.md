helm
=========

Role for helm installation

Example Playbook
----------------

    - hosts: srv-nodes
      become: yes
      roles:
        - helm

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb