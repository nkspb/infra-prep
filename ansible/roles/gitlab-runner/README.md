gitlab-runner
=========

Role for gitlab-runner installation and registration

Example Playbook
----------------

    - hosts: srv-nodes
      become: yes
      roles:
        - gitlab-runner

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb
