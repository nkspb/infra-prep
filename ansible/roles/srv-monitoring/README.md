srv-monitoring
=========

Role for deploying ESK, Grafana, Prometheus stack

Example Playbook
----------------

    - hosts: srv
      become: yes
      roles:
        - srv-monitoring

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb