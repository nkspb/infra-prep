app-prep
=========

Role for initial pg-app deployment

Example Playbook
----------------

    - hosts: srv
      become: yes
      roles:
        - pg-app

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb