kubectl
=========

Role for configuring k8s monitoring (fluentd, kube-state-metrics, node-exporter)

Example Playbook
----------------

    - hosts: srv
      become: yes
      roles:
        - yc

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb