docker
=========

Role for docker installation

Example Playbook
----------------

    - hosts: srv-nodes
      become: yes
      roles:
        - docker

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb
