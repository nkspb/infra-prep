kubectl
=========

Role for kubectl installation

Example Playbook
----------------

    - hosts: srv
      become: yes
      roles:
        - kubectl

License
-------

GPL-2.0

Author Information
------------------

Nikolay Komolov
github.com/nkspb
gitlab.com/nkspb