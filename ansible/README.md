**Playbook for srv-node components**<br />
***docker*** - monitoring and logging containers<br />
***gitlab-runner*** - build and deliver apps <br />
***helm*** - deploy apps to k8s<br />
***yc*** - manage Yandex Cloud and generate kubectl config<br />
***kubectl*** - control k8s